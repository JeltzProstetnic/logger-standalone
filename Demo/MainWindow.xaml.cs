﻿using MatthiasToolbox.Logging;
using MatthiasToolbox.Logging.Loggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // not required but very helpful if one wants to share logger settings among different loggers
        public DefaultLoggerSettings LoggerSettings { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            LoggerSettings = new DefaultLoggerSettings();

            Logger.Add(new ConsoleLogger(), LoggerSettings);
            Logger.Add(new PlainTextFileLogger(new System.IO.FileInfo("log.txt")), LoggerSettings);

            this.Log<INFO>("Hello World!");
        }
    }
}
